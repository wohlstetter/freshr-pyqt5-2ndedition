class myBaseClass:
    def __init__(self):
        print("myBaseClass.__init__ called")

class myChildClass1(myBaseClass):
    def __init__(self):
        super().__init__()
        print("myChildClass1.__init__ called")

class myChildClass2(myBaseClass):
    def __init__(self):
        super().__init__()
        print("myChildClass2.__init__ called")

class myGrandChildClass(myChildClass1, myChildClass2):
    def __init__(self):
        super().__init__()
        print("myGrandChildClass.__init__ called")
