class myImmortalClass:
    averageSpeed = 5
    averageStrength = 5

    def getLifespan(self):
        return "Immortal"

class myVampireClass(myImmortalClass):
    subSpeciesName = 'Vampire'
    averageSpeed = 15
    averageStrength = 15

    def __init__(self, name):
        self._name = name

    def hypnotizeVictim(self):
        print("Look into my eyes! You will do as I command...")

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. Lycans were made to serve us, Humans to feed us.")

class myLycanClass(myImmortalClass):
    subSpeciesName = 'Lycan'
    averageSpeed = 25
    averageStrength = 25

    def __init__(self, name):
        self._name = name

    def unleashClaws(self):
        print("My claws are strong enough to cut through steel. Clang, clang!")

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. No matter what any bat claims, Lycans and Vampires are equals.")

class myHybridDescendantOfMichaelClass(myVampireClass, myLycanClass):
    subSpeciesName = 'Hybrid'

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}.")
