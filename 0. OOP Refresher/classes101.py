class classDemo:
    def __new__(cls, *args, **kwargs):
        print ('__new__ is the constructor for a class')
        return object.__new__(cls)

    def __init__(self, args):
        print('__init__ is the initialiser for a class')

    def __del__(self):
        print('__del__ is the finaliser for a class')

    def greeting(self):
        print('__new__ or __init__? Now you know what get\'s called first :)')

class bananaClassDemo:
    def __init__(banana, name):
        banana._name = name

    def __del__(banana):
        print('This message is from the finaliser method of bananaClassDemo')

    def greeting(banana):
        print('Hello', banana._name)

class warthogClassDemo:
    def __init__(self, name):
        _name = name
        print('The value of _name inside the initialiser is', _name)

    def __del__(self):
        print('This message is from the finaliser method of warthogClassDemo')

    def greeting(self):
        """ This is here just to remind you that methods can also have docstrings :) """
        try:
            print('Hello', _name)
        except NameError:
            print('Few creatures are as forgetful as a warthog. I\'ve already forgotten your name!')

