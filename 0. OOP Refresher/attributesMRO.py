class myImmortalClass:
    subSpeciesName = 'Mutated human'

class myVampireClass(myImmortalClass):
    subSpeciesName = 'Vampire'

class myLycanClass(myImmortalClass):
    subSpeciesName = 'Lycan'

class myMichaelPostLycanBiteClass(myLycanClass):
    heightInCm = 186

class myTwiceBittenMichaelClass(myLycanClass, myVampireClass):          # Multiple inheritance scenario.
    heightInCm = 186
